# APIpizza

## Project description
Welcome to the tiny pizza delivery shop API which allows the user to deliver a yummy pizza with at least one and up to three toppings, eg. salami, mozzarella, mushrooms on your internet browser. Following the rules you can get the order history of all orders that were placed during the current session of the webserver. You can also delete all orders and the user will be informed.
## How to contribute
### Project structure
```shell
├── .git/
├── .venv/
├── tests/
│ ├── __init__.py
│ ├── test_pizzawebserver.py
├── webserver/
│ ├── __init__.py
│ ├── pizzawebserver.py
├── .gitignore
├── Pipfile
├── Pipfile.lock
├── README.md
```
### Git branch and merge
For contributing to the project please create your own branch and send a merge request.
### Prerequisites
Pipenv with a python version 3.9 is necessary. You can install pipenv with the command:
```shell
pip install pipenv
```
###Setup
In order to set up and run this project you have to clone the ```APIpizza``` project from gitlab with the following command:
```shell
git clone https://gitlab.com/hs-karlsruhe/ss2022/heju1030/apipizza.git
```
Install the required flask package:
```shell
pipenv install flask
```
### Run
Activate your python environment with:
```shell
pipenv shell
```
### How to use API
The API provides following functionalities:
#### 1. Welcome
By clicking [welcome](http://localhost:8080) or calling the URL:
```http
http://localhost:8080
```
the API will return you a welcome message as a json.
#### 2. Order pizza
By clicking [order](http://localhost:8080/order) or calling the URL:
```http
http://localhost:8080/order
```
you are able to order a pizza with at least one and up to three toppings.
By calling the URL:
```http
http://localhost:8080/order?topping1=Salami
```
you order a pizza with one topping, in this case salami. But you are also able to order up to three toppings.
By calling the URL:
```http
http://localhost:8080/order?topping1=Salami&topping2=Mozzarella&topping3=Mushrooms
```
you ordered a pizza with the maximum amount of toppings, in this case salami, mozzarella and mushrooms.

There is also the possibility to produce incorrect inputs.
If you order a pizza with a topping which is not available like the following example:
```http
http://localhost:8080/order?topping1=Corn
```
the API will give the following error message as output: "At least one of the toppings is not available. The following toppings are available: Mozzarella, Cheddar, Mushrooms, Tomatoes, Salami, Prosciutto, Spinach".
If the user does not choose the first topping like:
```http
http://localhost:8080/order?topping1=
```
the API will give the following error message: "Please choose at least the first topping."
#### 3. Orders
By clicking [orders](http://localhost:8080/orders) or calling the URL:
```http
http://localhost:8080/orders
```
the API will return you the history of all orders that were placed during the current session of the webserver as a json.
#### 4. Delete orders
By clicking [deleteOrders](http://localhost:8080/deleteOrders) or calling the URL:
```http
http://localhost:8080/deleteOrders
```
the API will delete all orders of the current session and the user will be informed that the orders were deleted successfully.
