from webserver.pizzawebserver import *


def test_hello_user():
    """Test hello user route."""
    with app.test_client() as test_app:
        response = test_app.get("/")
        assert response.status_code == 200
        assert response.json == {"message": "Hungry? Order a pizza with our Pizza Delivery API!"}


def test_order():
    """Test order route."""
    with app.test_client() as test_app:
        response = test_app.get("/order?topping1=Salami&topping2=Cheddar&topping3=Spinach")
        assert response.status_code == 200
        assert response.json == {"toppings": ["Salami", "Cheddar", "Spinach"]}

        response = test_app.get("/order?")
        assert response.status_code == 406
        assert response.json == {"message": "Please choose at least the first topping."}

        response = test_app.get("/order?topping1=pineapple")
        assert response.status_code == 404
        assert response.json == {"message": "At least one of the toppings is not available. The following toppings are available: Mozzarella, Cheddar, Mushrooms, Tomatoes, Salami, Prosciutto, Spinach"}


def test_orders():
    """Test orders route."""
    with app.test_client() as test_app:
        response = test_app.get("/orders")
        assert response.status_code == 200
        assert response.json == [{'toppings': ['Salami', 'Cheddar', 'Spinach']}]


def test_delete_orders():
    """Test delete orders route."""
    with app.test_client() as test_app:
        response = test_app.get("/deleteOrders")
        assert response.status_code == 200
        assert response.json == {"message": "The orders were deleted successfully"}
