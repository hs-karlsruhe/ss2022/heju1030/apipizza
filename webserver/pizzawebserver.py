from flask import Flask, request, jsonify

TOPPINGS = ["Mozzarella", "Cheddar", "Mushrooms", "Tomatoes", "Salami", "Prosciutto", "Spinach"]
ORDERS = []
app = Flask(__name__)


@app.route('/')
def hello_user():
    """Welcome page.

    :return: A simple greating
    """
    return jsonify(message="Hungry? Order a pizza with our Pizza Delivery API!"), 200


@app.route('/order')
def order():
    """Order a pizza with at least 1 and up to 3 toppings. The first topping (parameter topping1) must always be given.
        Toppings 2 and 3 are optional. If topping2 is not given, topping3 can sill be given as the second topping.

        :param topping1: First topping on the pizza
        :param topping2: Second topping on the pizza
        :param topping3: Third topping on the pizza
        :return: List of the toppings ordered on the pizza
        """
    topping1 = request.args.get('topping1')
    topping2 = request.args.get('topping2')
    topping3 = request.args.get('topping3')
    if topping1 is None or topping1 == "":
        return {
                   "message": "Please choose at least the first topping."
               }, 406
    topping_array = [topping1, topping2, topping3]
    topping_array_clean = list(filter(None, topping_array))
    i = 0
    while i < len(topping_array_clean):
        if not topping_array_clean[i] in TOPPINGS:
            return {
                       "message": "At least one of the toppings is not available. The following toppings are available: Mozzarella, Cheddar, Mushrooms, Tomatoes, Salami, Prosciutto, Spinach"
                   }, 404
        i = i+1
    order_dic = {"toppings": topping_array_clean}
    ORDERS.append(order_dic)
    return jsonify(toppings=topping_array_clean), 200


@app.route('/orders')
def orders():
    """Get order history.

    :return: A list of all orders
    """
    return jsonify(list(ORDERS)), 200


@app.route('/deleteOrders')
def delete_orders():
    """Delete all orders.

    :return: Confirmation of successful deletion process
    """
    ORDERS.clear()
    return jsonify(message="The orders were deleted successfully"), 200


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
